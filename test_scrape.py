import configparser
import json
import logging
import sys
import warnings
from datetime import datetime

import googlemaps
import jinja2
import pandas as pd
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_test_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 30)

config = configparser.ConfigParser()
config.read("ConfigFile.properties")

api_key = config.get("google", "api_key")
gmaps = googlemaps.Client(key=api_key)

root_url = "https://canastarosa.com"


def get_dom(url):
    """
    gets the DOM of the requested url using selenium. NA
    """
    driver.get(url)

get_dom("https://canastarosa.com/stores/reposteando/")
#get_dom("https://canastarosa.com/stores/pichurri/")
store_soup = BeautifulSoup(driver.page_source, "html.parser")

stars = store_soup.find_all("button", {"type": "button", "class": "icon active"})
if stars:
    rating = (stars[-1]).text.strip()
else:
    rating = "0"

print(rating)