"""
Author: Alain Moré Maceda
Scraper de tiendas canasta rosa MX
"""
import configparser
import json
import logging
import sys
import warnings
from datetime import datetime

import googlemaps
import jinja2
import pandas as pd
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 30)

config = configparser.ConfigParser()
config.read("ConfigFile.properties")

api_key = config.get("google", "api_key")
gmaps = googlemaps.Client(key=api_key)

root_url = "https://canastarosa.com"


def get_dom(url):
    """
    gets the DOM of the requested url using selenium. NA
    """
    driver.get(url)


def get_place_details(store_name):
    log.info("Getting Google info for %s", store_name)
    name = ""
    place_id = ""
    address = ""
    phone = ""
    website = ""
    rating = 0.0
    types = []
    lat = "0.0"
    lng = "0.0"
    opening_hours = []
    google_info = {}

    try:
        vendor = store_name.replace(' ', '%20')
        vendor = vendor.replace(',', '%2C')
        vendor = vendor.replace('á', 'a')
        vendor = vendor.replace('é', 'e')
        vendor = vendor.replace('í', 'i')
        vendor = vendor.replace('ó', 'o')
        vendor = vendor.replace('ú', 'u')
        vendor = vendor.replace('ñ', 'n')
        vendor = vendor.replace('\"', '')
        vendor = vendor.replace('\'', '%27')
        vendor = vendor.replace('#', '')
        vendor = vendor.replace('&', 'y')

        vendor_search = vendor + ",%20" + "Mexico"
        url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + vendor_search + \
            "&inputtype=textquery&fields=place_id,name,formatted_address,geometry,type,rating&key=" + api_key

        response = requests.get(url).json()
        log.info("Google Places response: %s", response.get("status"))

        if response and response.get("status") == "REQUEST_DENIED":
            log.info("ACCESO DENEGADO A API DE GOOGLE!! CANCELAR PROCESO!!")
            log.info(response)
            sys.exit(1)
        elif response and response.get("status") != "ZERO_RESULTS":

            url_contact = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + \
                response["candidates"][0].get("place_id") + "&fields=formatted_phone_number," \
                "website,opening_hours&key=" + \
                api_key

            response_contact = requests.get(url_contact).json()

            if 'name' in response["candidates"][0]:
                name = response["candidates"][0].get("name")
            if 'place_id' in response["candidates"][0]:
                place_id = response["candidates"][0].get("place_id")
            if 'formatted_address' in response["candidates"][0]:
                address = response["candidates"][0].get("formatted_address")
            if 'rating' in response["candidates"][0]:
                rating = response["candidates"][0].get("rating")
            if 'types' in response["candidates"][0]:
                types = json.dumps(response["candidates"][0].get("types"))
            if 'geometry' in response["candidates"][0]:
                if 'location' in response["candidates"][0].get("geometry"):
                    if 'lat' in response["candidates"][0].get("geometry").get("location"):
                        lat = response["candidates"][0].get(
                            "geometry").get("location").get("lat")
            if 'geometry' in response["candidates"][0]:
                if 'location' in response["candidates"][0].get("geometry"):
                    if 'lng' in response["candidates"][0].get("geometry").get("location"):
                        lng = response["candidates"][0].get(
                            "geometry").get("location").get("lng")
            if 'formatted_phone_number' in response_contact.get("result"):
                phone = response_contact.get(
                    "result").get("formatted_phone_number")
            if 'website' in response_contact.get("result"):
                website = response_contact.get("result").get("website")
            if 'opening_hours' in response_contact.get("result"):
                if 'weekday_text' in response_contact.get("result").get("opening_hours"):
                    opening_hours = json.dumps(response_contact.get(
                        "result").get("opening_hours").get("weekday_text"))

        else:
            log.info("NO encontrado en Google Places")

    except Exception as ex:
        print(ex)
        log.info("EXCEPTION!")
        log.exception("EXCEPTION")

    google_info["name"] = name
    google_info["place_id"] = place_id
    google_info["address"] = address
    google_info["rating"] = rating
    google_info["types"] = types
    google_info["lat"] = lat
    google_info["lng"] = lng
    google_info["phone"] = phone
    google_info["website"] = website
    google_info["opening_hours"] = opening_hours
    log.info(google_info)

    return google_info


def insert_in_sf(connection, store_info):
    insert = """
        INSERT INTO GLOBAL_ECOMMERCE.CANASTA_ROSA_TIENDAS
            (URL,STORE_NAME,STARS,RATINGS,OWNER,SHORT_DESC,LONG_DESC,SCHEDULE,CATEGORIES,TOTAL_PRODUCTS,
                GOOGLE_NAME,GOOGLE_PLACE_ID,GOOGLE_ADDRESS,GOOGLE_RATING,GOOGLE_TYPES,
                GOOGLE_LAT,GOOGLE_LNG,GOOGLE_PHONE,GOOGLE_WEBSITE,GOOGLE_OPENING_HOURS,
                SCRAPE_DATETIME)
        VALUES(
            '{{url}}',
            '{{store_name}}',
            {{stars}},
            {{ratings}},
            '{{owner}}',
            '{{short_desc}}',
            '{{long_desc}}',
            '{{schedule_json}}',
            '{{categories}}',
            {{total_products}},
            '{{google_name}}',
            '{{google_place_id}}',
            '{{google_address}}',
            {{google_rating}},
            '{{google_types}}',
            {{google_lat}},
            {{google_lng}},
            '{{google_phone}}',
            '{{google_website}}',
            '{{google_opening_hours}}',
            '{{scrape_datetime}}'
        )     
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**store_info)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def start_scraping(connection):
    """
    Scraping main function, orchestrates full process
    """
    get_dom(root_url + "/stores")
    store_listing_soup = BeautifulSoup(driver.page_source, "html.parser")
    pages_tag = store_listing_soup.find("ul", {"class": "page-numbers"})
    last_page = 0
    if pages_tag:
        last_page = int(
            (pages_tag.find_all("span", {"class": "button__index"})[-1]).text.strip())
        log.info("%i páginas totales", last_page)
    else:
        log.info("No se detectaron páginas")

    for i in range(679, last_page+1):
        get_dom(root_url + "/stores?&p=" + str(i))
        log.info("")
        log.info(">>>> Página %i de %i", i, last_page)
        wait.until(EC.visibility_of_element_located(
            (By.XPATH, '//div[@class="cr__stores-list"]')))
        store_listing_soup = BeautifulSoup(driver.page_source, "html.parser")

        stores = store_listing_soup.find_all(
            "div", {"class": "cr__stores-item"})
        for store_tag in stores:
            store_info = {}
            store_url = ""
            store_name = ""
            stars = 0
            ratings = 0
            owner = ""
            short_desc = ""
            long_desc = ""
            working_hours = []
            categories = ""
            total_products = 0
            google_info = {}

            url_tag = store_tag.find("a")
            if url_tag:
                store_suffix = url_tag["href"]
                store_url = root_url + store_suffix
                store_info["url"] = store_url
                get_dom(store_url)

                try:
                    wait.until(EC.visibility_of_element_located(
                        (By.XPATH, '//ul[@class="products__list"]')))
                except:
                    log.exception("Informacion incompleta:")

                store_soup = BeautifulSoup(driver.page_source, "html.parser")

                log.info("")

                store_name_tag = store_soup.find("h1", {"class": "title"})
                if store_name_tag:
                    store_name = store_name_tag.text.strip()
                    log.info("Tienda: %s", store_name)
                else:
                    log.info("No se encontró nombre")
                store_info["store_name"] = store_name

                ratings_container_tag = store_soup.find(
                    "div", {"class": "rating-container"})
                if ratings_container_tag:
                    ratings_tag = ratings_container_tag.find("span")
                    if ratings_tag:
                        ratings = int(ratings_tag.text.strip().replace(
                            "(", "").replace(")", ""))
                        log.info("Ratings: %i", ratings)
                    else:
                        log.info("No se encontró ratings")
                else:
                    log.info("No se encontró ratings container")
                store_info["ratings"] = ratings

                stars_list = store_soup.find_all(
                    "button", {"type": "button", "class": "icon active"})
                if stars_list:
                    stars = int((stars_list[-1]).text.strip())
                else:
                    stars = 0
                log.info("Estrellas: %i", stars)
                store_info["stars"] = stars

                owner_container_tag = store_soup.find(
                    "div", {"class": "creator__info"})
                if owner_container_tag:
                    owner_tag = owner_container_tag.find("p")
                    if owner_tag:
                        owner = owner_tag.text.strip()
                        log.info("Owner: %s", owner)
                    else:
                        log.info("No se encontró owner")
                else:
                    log.info("No se encontró owner container")
                store_info["owner"] = owner

                short_desc_tag = store_soup.find(
                    "p", {"class": "details__excerpt"})
                if short_desc_tag:
                    short_desc = short_desc_tag.text.strip()
                    log.info("Short Desc: %s", short_desc)
                else:
                    log.info("No se encontró short desc")
                store_info["short_desc"] = short_desc

                long_desc_tag = store_soup.find(
                    "div", {"class": "storeInfo__content"})
                if long_desc_tag:
                    long_desc_tmp = long_desc_tag.text.strip()
                    long_desc = long_desc_tmp.split("Horarios de la Tienda")[0]
                    log.info("Long Desc: %s", long_desc)
                else:
                    log.info("No se encontró long desc")
                store_info["long_desc"] = long_desc

                schedule_tags = store_soup.find_all(
                    "li", {"class": "schedules-list__item"})
                for schedule_tag in schedule_tags:
                    sch_dict = {}
                    sch_key = schedule_tag.find(
                        "div", {"class": "weekday"}).text.strip()
                    sch_value = schedule_tag.find("div", {"class": "schedule"}).text.strip(
                    ).replace(" ", "").replace("\n", "").replace("\u00e9", "e")
                    sch_dict[sch_key] = sch_value
                    working_hours.append(sch_dict)
                schedule_json = json.dumps(working_hours)
                log.info(schedule_json)
                store_info["schedule_json"] = ''.join(
                    schedule_json).replace("'", "")

                categories_tags = store_soup.find_all(
                    "a", {"class": "category__link"})
                for category_tag in categories_tags:
                    # categories.append(category_tag.text.strip())
                    if category_tag.text.strip() != 'Todos':
                        categories = categories + category_tag.text.strip() + ", "
                if not categories:
                    categories = "NA"
                else:
                    size = len(categories)
                    categories = categories[:size - 2]
                log.info(categories)
                #store_info["categories"] = ''.join(categories).replace("'", "")                
                store_info["categories"] = categories.replace("'", "")

                products_list_tag = store_soup.find(
                    "ul", {"class": "products__list"})
                if products_list_tag:
                    products_list = products_list_tag.find_all(
                        "li", {"class": "product"})
                    if products_list:
                        total_products = len(products_list)
                        log.info("Total de productos: %i", total_products)
                    else:
                        log.info("No se encontraron productos")
                else:
                    log.info("No se encontro la lista de productos")
                store_info["total_products"] = total_products

            log.info(store_info)
            google_info = get_place_details(store_name)
            store_info["google_name"] = google_info["name"].replace("'", "")
            store_info["google_place_id"] = google_info["place_id"]
            store_info["google_address"] = google_info["address"].replace(
                "'", "")
            store_info["google_rating"] = google_info["rating"]
            store_info["google_types"] = ''.join(
                google_info["types"]).replace("'", "")
            store_info["google_lat"] = google_info["lat"]
            store_info["google_lng"] = google_info["lng"]
            store_info["google_phone"] = google_info["phone"].replace("'", "")
            store_info["google_website"] = google_info["website"].replace(
                "'", "")
            store_info["google_opening_hours"] = ''.join(
                google_info["opening_hours"]).replace("'", "\"")
            store_info["scrape_datetime"] = datetime.now()
            log.info("Full info")
            log.info(store_info)

            insert_in_sf(connection, store_info)
            log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS STARTED OK!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "ECOMMERCE",
        "role": "GLOBAL_ECOMMERCE_WRITE_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    try:
        start_scraping(connection)
        print("PROCESS ENDED OK!")
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
