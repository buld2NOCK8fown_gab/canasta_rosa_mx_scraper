import googlemaps
import configparser
import requests

config = configparser.ConfigParser()
config.read("ConfigFile.properties")
api_key = config.get("google", "api_key")
gmaps = googlemaps.Client(key=api_key)


# url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=Museum%20of%20Contemporary%20Art%20Australia&inputtype=textquery&key=" + api_key
vendor_search = "Cornstars" + ",%20" + "Mexico"
# Para solo 1 lugar exacto!
url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + vendor_search + \
        "&inputtype=textquery&fields=place_id,name,formatted_address,geometry,type,rating&key=" + api_key

url2 = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + \
                            "ChIJVe0SKhr_0YURRNYyhkjCvWs" + "&fields=formatted_phone_number," \
                                                                        "website,opening_hours&key=" + \
                            api_key
payload = {}
headers = {}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
